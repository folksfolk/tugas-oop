<?php
    require_once ('Animal.php');
    require_once ('Ape.php');
    require_once ('Frog.php');

    $sheep = new Animal("shaun");

    echo $sheep->name;
    echo "<br>";
    echo $sheep->legs;
    echo "<br>";
    echo $sheep->cold_blooded;
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell();
    echo "<br>";
    echo $sungokong->name;
    echo "<br>";
    echo $sungokong->legs;
    echo "<br>";
    echo $sungokong->cold_blooded;
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump();
    echo $kodok->name;
    echo "<br>";
    echo $kodok->legs;
    echo "<br>";
    echo $kodok->cold_blooded;
    echo "<br>";
?>